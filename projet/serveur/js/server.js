const express = require('express');
const app = express();
const path = require('path');

// ajout de socket.io
const server = require('http').Server(app)
const io = require('socket.io')(server)
server.listen(3000, '10.224.2.5');

//Connexion à la base de données
let connect = false;
let db;
const MongoClient = require("mongodb").MongoClient;
const url = 'mongodb://10.224.2.7:27017';
MongoClient.connect(url, function (error, client) {
    if (error) return funcCallback(error);
    connect = true;
    console.log("Connecté à la base de données 'mappy'");
    db = client.db("mappy");
});
//Fin


app.get('/', function (request, response) {
    response.sendFile(path.join(__dirname, '/../../client/html/view.html'));  //Affiche à la racine la page view.js
});



io.sockets.on('connection', (socket) => {
    console.log(`Connecté au client ${socket.id}`);
    if (connect == true) {
            db.collection("Login").find({}, { projection: { _id: 0 } }).toArray(function (err, result) {
            console.log(result[1]);
            socket.emit('database', result[1]);

        });

        /*db.collection("Login").insertOne({ name: "Abdel", adress: "Lille" }), function (err, res) {
            if (err) throw err;
            console.log("1 document inserted");
        };*/

    }
    else{
        console.log("ERROR DATA BASE CRASH");
    }


})


